module.exports = {
  colorBackground: 'white',
  colorBackgroundHue: '#f9f9f9',
  colorPrimary: '#000000',
  colorSecondary: 'gainsboro',
  colorBorder: 'gainsboro',
  colorShadow: 'rgba(0, 0, 0, 0.1)',
  colorTextQuaternary: 'rgba(0, 0, 0, 0.50)',

  colorSuccess: 'green',
  colorError: 'red',
  colorWarning: '#ffc300',

  colorText: '#696969',
  colorTextReverse: 'white',

  fontInterface:
    "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'",
  fontSizeLarge: '16px',
  fontSizeBase: '14px',
  fontSizeBaseSmall: '12px',

  gridUnit: '4px',

  borderRadius: '3px',
  borderWidth: '1px',
  borderStyle: 'solid',

  headerPaddingVertical: '0',
}
